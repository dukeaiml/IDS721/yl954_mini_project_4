# yl954_mini_project_4

## Demo Video
The demo video is called ```DemoVideo.webm``` and is in the root directory of this repository

## Install Docker [link](https://docs.docker.com/engine/install/ubuntu/)
1. Run the following command to uninstall all conflicting packages
```
for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove $pkg; done
```
2. Set up Docker's apt repository
```
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```
3. Install the Docker packages
```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
4. Verify
```
sudo docker run hello-world
```

## Create Rust Actix Web Application
1. ```cargo new actix_web_app```
2. Add Actix-web to ```Cargo.toml```
```
[dependencies]
actix-web = "4.5.1"
```
3. Replace the content of ```src/main.rs``` with Actix web server code
4. Verify by using ```cargo run```\
**(This project implemented a simple adder.)**

## Create Dockerfile
```
# Use an official Rust image 
# It's best to match your own version of rust
FROM rust:1.75 as builder

# Create a new empty shell project
RUN USER=root cargo new mini4
WORKDIR /mini4

# Copy the manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# This is a dummy build to get the dependencies cached
RUN cargo build --release
RUN rm src/*.rs

# Now that the dependencies are built, copy your source code
COPY ./src ./src

# Build for release
RUN rm ./target/release/deps/mini4*
RUN cargo build --release

# Final stage
# Be careful: Different versions of debian support different versions of glibc
FROM debian:bookworm-slim
COPY --from=builder /mini4/target/release/mini4 .
ENV ROCKET_ADDRESS=0.0.0.0
CMD ["./mini4"]

```

## Build Docker Image
```sudo docker build -t mini4 .```

## Delete Docker Image
1. Get image ID by ```sudo docker images```
2. Delete image by ```sudo docker rmi <image_ID>```

## Create and run Container Locally
```sudo docker run (-d) -p 8080:8080 mini4```\
Note: ```-d: run in the background```

## Start or Stop or Delete Container
1. View all containers: ```sudo docker ps -a```
2. Start: ```sudo docker start container_id```
3. Stop: ```sudo docker stop container_id```
4. Delete: ```sudo docker rm container_id```
