use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::Deserialize;

#[derive(Deserialize)]
struct AddParams {
    a: Option<i32>,
    b: Option<i32>,
}

async fn add(params: web::Query<AddParams>) -> impl Responder {
    match (params.a, params.b) {
        (Some(a), Some(b)) => {
            let sum = a + b;
            HttpResponse::Ok().body(format!("{} + {} = {}", a, b, sum))
        },
        _ => HttpResponse::BadRequest().body("Missing or invalid parameters!"),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().route("/", web::get().to(add)))
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
